﻿
function generateHourlyHitsChart(hourlyCount){
  var hourlyTitles=[];
  for (var index = 0; index < hourlyCount.length; ++index) {
    hourlyTitles[index]="Hour "+index;
  }
  
  var chart,
      categories = hourlyTitles,
      serie1 = hourlyCount,
      serie2 = hourlyCount,
      $aapls;
  
    chart = new Highcharts.Chart({
      chart: {
        renderTo: 'importantchart',
        type: 'column',
        backgroundColor: 'transparent',
        height: 170,
        marginLeft: 3,
        marginRight: 3,
        marginBottom: 0,
        marginTop: 0
      },
      title: {
        text: ''
      },
      xAxis: {
        lineWidth: 0,
        tickWidth: 0,
        labels: { 
          enabled: false 
        },
        categories: categories
      },
      yAxis: {
        labels: { 
          enabled: false 
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      series: [{
        name: 'Hits',
        data: serie1
      }, {
        name: 'Hits',
        color: '#fff',
        type: 'line',
        data: serie2
      }],
      credits: { 
        enabled: false 
      },
      legend: { 
        enabled: false 
      },
      plotOptions: {
        column: {
          borderWidth: 0,
          color: '#b2c831',
          shadow: false
        },
        line: {
          marker: { 
            enabled: false 
          },
          lineWidth: 3
        }
      },
      tooltip: { 
        enabled: true
      }
    });
      
}
