var express = require('express');
var router = express.Router();
var app = express();
var path = require("path");
var bodyParser = require('body-parser');
var auth = require('basic-auth');
//var apiClient=require('./src/controllers/apiclient');
const env = require('./src/config/env/environment');
const logger = require('./src/config/logging');

logger.info('Logs-Dashboard Portal 1.0.SNAPSHOT');

app.use('/dashboard', express.static(__dirname + '/assets'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//app.get('/dashboard', function (req, res) {
  //  res.sendFile(__dirname + '/assets/index.html');
//});

app.use(require('./src/controllers'));

app.listen(env.server.port, () => {
    logger.info('listening on '+ env.server.port);
});



