var mongoose = require('../config/db')
var Schema = mongoose.Schema;

var ratingSchema = new Schema({
    userId: String,
    rating: Number,
    review : String,
    ratingDate: Date,
    isUpdatedOnAppStore:String,
    appType: String
});

var Rating = mongoose.model('Rating', ratingSchema);
module.exports = Rating;

