var mongoose = require('../config/db')

var Schema = mongoose.Schema;

var notificationSchema = new Schema({
    userId: String,
    userName: String,
    subject: String,
    body: String,
    type: String,
    channel: String,
    timestamp: Date,
    timestampNum: Number,
    isImportant: String,
    isRead: String,
    status: String,
    msisdn: String,
    service: String,
    amount: String,
    transactionId: String,
    transactionType:String,
    screen: String,
    icon: String
});

notificationSchema.index({'$**': 'text'});

var Notification = mongoose.model('Notification', notificationSchema);
module.exports = Notification;

