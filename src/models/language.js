var mongoose = require('../config/db')
var Schema = mongoose.Schema;

var languageSchema = new Schema({
    code : { type: String, required: true},
    name : { type: String, required: true},
    country : String,
    status : String
});

var Language = mongoose.model('Language', languageSchema);
module.exports = Language;

