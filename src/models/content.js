var mongoose = require('../config/db');
var Schema = mongoose.Schema;

var autoPopulate= require('mongoose-autopopulate');

var contentSchema = new Schema();
contentSchema.add({
	type: String,
    language: String,
    title: String,
	description: String,
	text: String,
    screen: String,
    order: Number,
	banner: String,
	image: String,
	thumbnail: String,
	link: String,
	forAll: String,
	forServices: String,
	startDate: String,
	endDate: String,
	publishedDate: String,
	publishedBy: String,
	status: String,
	group:String,
	contentId:String,
	parentId: {type: Schema.Types.ObjectId, ref: "Content"},
	subContents:[{type: Schema.Types.ObjectId, ref: "Content"}]
});

contentSchema.plugin(autoPopulate);

var Content = mongoose.model('Content', contentSchema);
module.exports = Content;