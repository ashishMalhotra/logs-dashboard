var mongoose = require('../config/db')
var Schema = mongoose.Schema;

var screenSchema = new Schema({
    name : { type: String, required: true},
    type : String,
    status : String
});

var Screen = mongoose.model('Screen', screenSchema);
module.exports = Screen;

