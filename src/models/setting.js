var mongoose = require('../config/db')
var Schema = mongoose.Schema;

var settingSchema = new Schema({
    userId: String,
    allowNotification: String,
    language : String,
    emailForTransaction: String,
    twoStepAuthentication:String,
    appType: String,
    updatedTimeStamp: Date
});

var Setting = mongoose.model('Setting', settingSchema);
module.exports = Setting;

