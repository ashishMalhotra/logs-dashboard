var mongoose = require('../config/db')

var Schema = mongoose.Schema;

var requestMoneySchema = new Schema({
    userId: String,
    userName: String,
    fromUserId: String,
    fromUserName: String,
    type: String,
    channel: String,
    timestamp: Date,
    timestampNum: Number,
    amount: Number,
    status: String,
    message: String,
    remarks: String,
    updatedTimeStamp: Date
});

var RequestMoney = mongoose.model('RequestMoney', requestMoneySchema);
module.exports = RequestMoney;

