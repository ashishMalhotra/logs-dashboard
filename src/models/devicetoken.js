var mongoose = require('../config/db')
var Schema = mongoose.Schema;

var deviceTokenSchema = new Schema({
    userId: String,
    token : String,
    language: String,
    appVersion: String,
    updatedTimestamp: Date,
    appType: String
});

var DeviceToken = mongoose.model('DeviceToken', deviceTokenSchema);
module.exports = DeviceToken;

