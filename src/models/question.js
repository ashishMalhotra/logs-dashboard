var mongoose = require('../config/db')
var Schema = mongoose.Schema;

var questionSchema = new Schema({
    language : { type: String, required: true},
    question : { type: String, required: true},
    questionId : String,
    order: Number,
    status : String
});

var Question = mongoose.model('Question', questionSchema);
module.exports = Question;

