var mongoose = require('../config/db')
var Schema = mongoose.Schema;

var addToFavoritesSchema = new Schema({
    userId:String,
    channel:String,
    amount: Number,
    serviceId: Number,
    serviceName: String,
    contentId: String,
    info: String,
    senderMsisdn: String,
    senderName: String,
    receiverMsisdn: String,
    receiverName: String,
    transacionDate: String,
    timestampNum: String,
    status : String,
    accountNo: String
});

var AddToFavorites = mongoose.model('AddToFavorites', addToFavoritesSchema);
module.exports = AddToFavorites;

