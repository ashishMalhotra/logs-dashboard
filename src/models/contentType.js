var mongoose = require('../config/db')

var Schema = mongoose.Schema;

var contentTypeSchema = new Schema({
    type : String,
    name : String,
    status : String
});

var ContentType = mongoose.model('ContentType', contentTypeSchema);
module.exports = ContentType;

