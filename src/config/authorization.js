const logger = require('../config/logging');

exports.isAuthenticated =function(req, res, next) {
    var request = req.method+' ' + req.protocol + '://' + req.get('host') + req.originalUrl+ ' '+ JSON.stringify(req.body) +']';
    logger.info("Request: "+request);
    const auth = {login: 'Agent', password: 'Agent'};
    const b64auth = (req.headers.authorization || '').split(' ')[1] || '';
    const [login, password] = new Buffer(b64auth, 'base64').toString().split(':');
    
    if (!login || !password || login !== auth.login || password !== auth.password) {
        res.set('WWW-Authenticate', 'Basic realm="nope"');
        res.status(401);
        res.json(failed());
        return;
    }
    return next();
  }

function failed(){
    let apiResponse={};
    apiResponse.statusCode=145;
    apiResponse.status="Failure";
    apiResponse.message="Unauthorized";
    apiResponse.object=null;
    return apiResponse;
}