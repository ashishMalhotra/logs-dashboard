var express = require('express');
var router = express.Router();
var context='/dashboard';

router.use(context, require('./apihits'));
router.use(context, require('./search'));
router.use(context, require('./home'));

module.exports = router;