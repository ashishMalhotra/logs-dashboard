var express = require('express');
var router = express.Router();
var basePath= '/apihits';
const logger = require('../config/logging');
var shell = require('shelljs');
const env = require('../config/env/environment');

const logsBasePath=env.logs.basePath;
var coreReqLogsPath=logsBasePath+"/mfscore/request/request.log";
var coreResLogsPath=logsBasePath+"/mfscore/response/response.log";
var inboundReqLogsPath=logsBasePath+"/inbound/request/request.log";
var inboundResLogsPath=logsBasePath+"/inbound/response/response.log";
var billerReqLogsPath=logsBasePath+"/billergw/core/request/request.log";
var billerResLogsPath=logsBasePath+"/billergw/core/response/response.log";
var aggReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/request/request.log";
var aggResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/response/response.log";
var thirdPartyReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/thirdParty/request/request.log";
var thirdPartyResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/thirdParty/response/response.log";
var callBackReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/callback/request/request.log";
var callBackResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/callback/response/response.log";


router.get(basePath+'/all/count', (req, res) => { 
shell.exec("grep 'REQ' "+coreReqLogsPath+" | wc -l", {silent:true}, function(status, output) {
    var result = output.toString().replace(/\r?\n|\r/g, "");
    res.json(result);
  });
});

module.exports = router;