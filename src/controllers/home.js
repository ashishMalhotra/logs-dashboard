var express = require('express');
var router = express.Router();
var basePath= '/home';
const logger = require('../config/logging');
var shell = require('shelljs');
const env = require('../config/env/environment');

const logsBasePath=env.logs.basePath;
var coreReqLogsPath=logsBasePath+"/mfscore/request/request.log";
var coreResLogsPath=logsBasePath+"/mfscore/response/response.log";
var inboundReqLogsPath=logsBasePath+"/inbound/request/request.log";
var inboundResLogsPath=logsBasePath+"/inbound/response/response.log";
var billerReqLogsPath=logsBasePath+"/billergw/core/request/request.log";
var billerResLogsPath=logsBasePath+"/billergw/core/response/response.log";
var aggReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/request/request.log";
var aggResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/response/response.log";
var thirdPartyReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/thirdParty/request/request.log";
var thirdPartyResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/thirdParty/response/response.log";
var callBackReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/callback/request/request.log";
var callBackResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/callback/response/response.log";
var saldomoboResLogsPath=logsBasePath+"/billergw/aggregator/agg-saldomobo/thirdParty/response/response.log";
var bimasaktiResLogsPath=logsBasePath+"/billergw/aggregator/agg-bimasakti/thirdParty/response/response.log";

router.get(basePath+'/data', (req, res) => {
var response={
   hitCount:{
     core:{},
     coreCurrentHour:{},
     inbound:{},
     biller:{},
     billerCallBack:{},
     aggregator:{},
     thirdParty:{},
     channel:{},
     gatewayTimeOut:{},
     hourlyCountForCore:[],
     saldomoboErrorCodes:[],
     bimasaktiErrorCodes:[]
   }
};  

var currentDate=new Date().toISOString().
  replace(/T/, ' ').      // replace T with a space
  replace(/\..+/, '');
var datePart=currentDate.split(" ")[0];

var nDate = new Date().toLocaleString('en-US', {
  timeZone: 'Asia/Jakarta'
});
var currentHour=parseInt(nDate.split(" ")[1].split(":")[0]);
if(currentHour!=12 && nDate.split(" ")[2]=="PM"){
  currentHour+=12;
}

  shell.exec("LANG=C grep 'REQ|' "+coreReqLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
    var result = output.toString().replace(/\r?\n|\r/g, "");
    response.hitCount.core.total=result;
    shell.exec("LANG=C grep -i 'Success' "+coreResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
      var result = output.toString().replace(/\r?\n|\r/g, "");
      response.hitCount.core.success=result;
      response.hitCount.core.failed=(response.hitCount.core.total-result)+"";  

      shell.exec("LANG=C grep 'REQ|' "+inboundReqLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
        var result = output.toString().replace(/\r?\n|\r/g, "");
        response.hitCount.inbound.total=result;
        shell.exec("LANG=C grep -i 'Success' "+inboundResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
          var result = output.toString().replace(/\r?\n|\r/g, "");
          response.hitCount.inbound.success=result;
          response.hitCount.inbound.failed=(response.hitCount.inbound.total-result)+"";  

          shell.exec("LANG=C grep 'REQ|' "+billerReqLogsPath+"."+datePart+"-* | grep -v '/callBack' |wc -l", {silent:true}, function(status, output) {
            var result = output.toString().replace(/\r?\n|\r/g, "");
            response.hitCount.biller.total=result;
            shell.exec("LANG=C grep -i 'Success' "+billerResLogsPath+"."+datePart+"-* | grep -v '/callBack' | wc -l", {silent:true}, function(status, output) {
              var result = output.toString().replace(/\r?\n|\r/g, "");
              response.hitCount.biller.success=result;
              response.hitCount.biller.failed=(response.hitCount.biller.total-result)+"";  

              shell.exec("LANG=C grep 'REQ|' "+aggReqLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                var result = output.toString().replace(/\r?\n|\r/g, "");
                response.hitCount.aggregator.total=result;
                shell.exec("LANG=C grep -i 'Success' "+aggResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                  var result = output.toString().replace(/\r?\n|\r/g, "");
                  response.hitCount.aggregator.success=result;
                  response.hitCount.aggregator.failed=(response.hitCount.aggregator.total-result)+"";  

                  shell.exec("LANG=C grep 'REQ|' "+thirdPartyReqLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                    var result = output.toString().replace(/\r?\n|\r/g, "");
                    response.hitCount.thirdParty.total=result;
                    shell.exec("LANG=C grep -i 'Success' "+thirdPartyResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                      var result = output.toString().replace(/\r?\n|\r/g, "");
                      response.hitCount.thirdParty.success=result;
                      response.hitCount.thirdParty.failed=(response.hitCount.thirdParty.total-result)+"";  
                    
                      shell.exec('LANG=C grep "|21|" '+coreReqLogsPath+'.'+datePart+'-* | wc -l', {silent:true}, function(status, output) {
                        var result = output.toString().replace(/\r?\n|\r/g, "");
                        response.hitCount.channel.ussd=result;
                        shell.exec('LANG=C grep "|22|" '+coreReqLogsPath+'.'+datePart+'-* | wc -l', {silent:true}, function(status, output) {
                          var result = output.toString().replace(/\r?\n|\r/g, "");
                          response.hitCount.channel.web=result;
                          shell.exec('LANG=C grep "|23|" '+coreReqLogsPath+'.'+datePart+'-* | wc -l', {silent:true}, function(status, output) {
                            var result = output.toString().replace(/\r?\n|\r/g, "");
                            response.hitCount.channel.app=result;
                            
                            logger.info(coreReqLogsPath+'.'+datePart+'-'+currentHour);
                            shell.exec('LANG=C grep "REQ|" '+coreReqLogsPath+'.'+datePart+'-'+currentHour+' | wc -l', {silent:true}, function(status, output) {
                              var result = output.toString().replace(/\r?\n|\r/g, "");
                              response.hitCount.coreCurrentHour.total=result;
                              shell.exec('LANG=C grep -i "Success" '+coreResLogsPath+'.'+datePart+'-'+currentHour+' | wc -l', {silent:true}, function(status, output) {
                                var result = output.toString().replace(/\r?\n|\r/g, "");
                                response.hitCount.coreCurrentHour.success=result;
                                response.hitCount.coreCurrentHour.failed=(response.hitCount.coreCurrentHour.total-result)+"";            
                                
                                   shell.exec("LANG=C grep 'REQ|' "+billerReqLogsPath+"."+datePart+"-* | grep '/callBack' | wc -l", {silent:true}, function(status, output) {
                                    var result = output.toString().replace(/\r?\n|\r/g, "");
                                    response.hitCount.billerCallBack.total=result;
                                      shell.exec("LANG=C grep -i 'Success' "+billerResLogsPath+"."+datePart+"-* | grep '/callBack' | wc -l", {silent:true}, function(status, output) {
                                      var result = output.toString().replace(/\r?\n|\r/g, "");
                                      response.hitCount.billerCallBack.success=result;
                                      response.hitCount.billerCallBack.failed=(response.hitCount.billerCallBack.total-result)+"";  
                                      
                                      shell.exec("LANG=C grep -ir 'gateway time' --include '*re*.'"+datePart+"-* "+logsBasePath+"/ | wc -l", {silent:true}, function(status, output) {
                                        var result = output.toString().replace(/\r?\n|\r/g, "");
                                        response.hitCount.gatewayTimeOut.total=result;

                                        shell.exec("LANG=C wc -l "+coreReqLogsPath+"."+datePart+"-* | cut -d '-' -f5|  xargs | sed -e 's/ /, /g'", {silent:true}, function(status, output) {
                                          var hours=output.split(",");
                                            shell.exec("wc -l "+coreReqLogsPath+"."+datePart+"-* |  cut -b 1-9 | xargs | sed -e 's/ /, /g'", {silent:true}, function(status, output) {
                                            var result = output.toString().replace(/\r?\n|\r/g, "");
                                            var hourCount=output.split(",");
                                            
                                            hours.pop();
                                            hours.pop();
                                            hourCount.pop();

                                            var map=[];

                                            for (var index = 0; index < hours.length; ++index) {
                                              if(map[parseInt(hours[index])]==null){
                                                map[parseInt(hours[index])]=parseInt(hourCount[index]);
                                              }else{
                                                var value=map[parseInt(hours[index])];
                                                map[parseInt(hours[index])]=parseInt(hourCount[index])+parseInt(value);
                                              }
                                            }

                                            response.hitCount.hourlyCountForCore=map;
                                            var saldomoboErrorCodes=[];
                                            shell.exec("LANG=C grep '<res:ResultCode>0</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                              var result = output.toString().replace(/\r?\n|\r/g, "");
                                              saldomoboErrorCodes.push({errorCode:"Success",description:"Transaction Successful.",count:result});
                                              
                                            shell.exec("LANG=C grep '<res:ResultCode>2006</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                              var result = output.toString().replace(/\r?\n|\r/g, "");
                                              saldomoboErrorCodes.push({errorCode:"2006",description:"Balance insufficient",count:result});
                                              
                                              shell.exec("LANG=C grep '<res:ResultCode>SM50019</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                var result = output.toString().replace(/\r?\n|\r/g, "");
                                                saldomoboErrorCodes.push({errorCode:"SM50019",description:"The third-party system fails to verify the recharge number",count:result});
                                                
                                                shell.exec("LANG=C grep '<res:ResultCode>UMC65001</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                  var result = output.toString().replace(/\r?\n|\r/g, "");
                                                  saldomoboErrorCodes.push({errorCode:"UMC65001",description:"The recharge MSISDN is incorrect",count:result});
                                                
                                                  shell.exec("LANG=C grep '<res:ResultCode>SM50017</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                    var result = output.toString().replace(/\r?\n|\r/g, "");
                                                    saldomoboErrorCodes.push({errorCode:"SM50017",description:"The system receives a failure response from the third-party system, an AG policy is configured to reverse the transaction, and the reversal is successful",count:result});
                                                  
                                                    shell.exec("LANG=C grep '<res:ResultCode>SM65002</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                      var result = output.toString().replace(/\r?\n|\r/g, "");
                                                      saldomoboErrorCodes.push({errorCode:"SM65002",description:"A request is consecutively initiated multiple times in a short period of time",count:result});
                                                    
                                                      shell.exec("LANG=C grep '<res:ResultCode>-1</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                        var result = output.toString().replace(/\r?\n|\r/g, "");
                                                        saldomoboErrorCodes.push({errorCode:"-1",description:"System internal error",count:result});
                                                      
                                                        shell.exec("LANG=C grep '<res:ResultCode>2006</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                          var result = output.toString().replace(/\r?\n|\r/g, "");
                                                          saldomoboErrorCodes.push({errorCode:"SM50016",description:"The system receives a failure response from the third-party system and an AG policy is configured not to process this fault",count:result});
                                                          response.hitCount.saldomoboErrorCodes=saldomoboErrorCodes;
                                                        
                                                          var bimasaktiErrorCodes=[];
                                                          shell.exec("LANG=C grep '<string>00</string>' "+bimasaktiResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                            var result = output.toString().replace(/\r?\n|\r/g, "");
                                                            bimasaktiErrorCodes.push({errorCode:"Success",description:"Success/Pending Transaction",count:result});
                                                          
                                                          shell.exec("LANG=C grep '<string>15</string>' "+bimasaktiResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                            var result = output.toString().replace(/\r?\n|\r/g, "");
                                                            bimasaktiErrorCodes.push({errorCode:"15",description:"Duplicate Transaction",count:result});
                                                            
                                                            shell.exec("LANG=C grep '<string>XX</string>' "+bimasaktiResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                              var result = output.toString().replace(/\r?\n|\r/g, "");
                                                              bimasaktiErrorCodes.push({errorCode:"XX",description:"THE NUMINAL VALUE OF PAID IS NOT ACCURATE",count:result});
                                                              
                                                              shell.exec("LANG=C grep '<string>07</string>' "+bimasaktiResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                                var result = output.toString().replace(/\r?\n|\r/g, "");
                                                                bimasaktiErrorCodes.push({errorCode:"07",description:"Service is being interrupted",count:result});
                                                              
                                                                shell.exec("LANG=C grep '<string>10</string>' "+bimasaktiResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                                  var result = output.toString().replace(/\r?\n|\r/g, "");
                                                                  bimasaktiErrorCodes.push({errorCode:"10",description:"EXT: UNKNOWN_NUMBER",count:result});
                                                                
                                                                  shell.exec("LANG=C grep '<string>13</string>' "+bimasaktiResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                                    var result = output.toString().replace(/\r?\n|\r/g, "");
                                                                    bimasaktiErrorCodes.push({errorCode:"13",description:"The transaction is rejected because the system is doing a cut off process (23.55 - 00.10)",count:result});
                                                                  
                                                                    shell.exec("LANG=C grep '<string>58</string>' "+bimasaktiResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                                      var result = output.toString().replace(/\r?\n|\r/g, "");
                                                                      bimasaktiErrorCodes.push({errorCode:"58",description:"EXT: TRANSACTION PROCESSES CAN NOT BE DONE",count:result});
                                                                    
                                                                      shell.exec("LANG=C grep '<string>24</string>' "+bimasaktiResLogsPath+"."+datePart+"-* | wc -l", {silent:true}, function(status, output) {
                                                                        var result = output.toString().replace(/\r?\n|\r/g, "");
                                                                        bimasaktiErrorCodes.push({errorCode:"24",description:"There are still transactions with the same customer id that is in the process",count:result});
                                                                        response.hitCount.bimasaktiErrorCodes=bimasaktiErrorCodes;
                                                                        res.json(response); 
                                                                      });
                                                                    });
                                                                  });
                                                                });
                                                              });
                                                            });
                                                          });
                                                        });
                                                      });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                             
                                          });
                                        });                                 
                                      });

                                    });
                                  });
                                   
                              });
                            });
                            
                          });
                        });
                      });
                    
                    });
                  });

                });
              });

            });
          });

        });
      });

    });
  });

});

module.exports = router;
