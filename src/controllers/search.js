var express = require('express');
var router = express.Router();
var basePath= '/search';
const logger = require('../config/logging');
var shell = require('shelljs');
const env = require('../config/env/environment');

const logsBasePath=env.logs.basePath;
var coreReqLogsPath=logsBasePath+"/mfscore/request/request.log";
var coreResLogsPath=logsBasePath+"/mfscore/response/response.log";
var inboundReqLogsPath=logsBasePath+"/inbound/request/request.log";
var inboundResLogsPath=logsBasePath+"/inbound/response/response.log";
var billerReqLogsPath=logsBasePath+"/billergw/core/request/request.log";
var billerResLogsPath=logsBasePath+"/billergw/core/response/response.log";
var aggReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/request/request.log";
var aggResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/response/response.log";
var thirdPartyReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/thirdParty/request/request.log";
var thirdPartyResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/thirdParty/response/response.log";
var callBackReqLogsPath=logsBasePath+"/billergw/aggregator/agg-*/callback/request/request.log";
var callBackResLogsPath=logsBasePath+"/billergw/aggregator/agg-*/callback/response/response.log";
var saldomoboResLogsPath=logsBasePath+"/billergw/aggregator/agg-saldomobo/thirdParty/response/response.log";
var bimasaktiResLogsPath=logsBasePath+"/billergw/aggregator/agg-bimasakti/thirdParty/response/response.log";

router.post(basePath+'/logs', (req, res) => {
var searchStr=req.body.search;

var response={
   inbound:{},
   core:{},
   biller:{},
   aggregator:{},
   thirdParty:{},
   callBack:{}
};

var currentDate=new Date().toISOString().
replace(/T/, ' ').      // replace T with a space
replace(/\..+/, '');

var searchDate=req.body.searchDate;
var datePart=null;
if(searchDate==null || searchDate==""){
  datePart=currentDate.split(" ")[0];
  datePart="."+datePart+"-*";
}else{
  datePart="."+searchDate+"-*";
}

var searchStrings=searchStr.split("|");

  shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+inboundReqLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
    var result = output.toString().replace(/\r?\n|\r/g, "");
    searchStrings.forEach(function(string) {
      result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
    });
    var inboundRequests=result.replace(new RegExp("REQ", 'g'),"<br><span style='background:#5cb85c'>REQ</span>");
    response.inbound.requests=inboundRequests;

    shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+inboundResLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
      var result = output.toString().replace(/\r?\n|\r/g, "");
      searchStrings.forEach(function(string) {
        result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
      });
      result=result.replace(new RegExp("Success", 'g'),"<span style='color:green;font-weight: bold;'>Success</span>");
      var inboundResponses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
      response.inbound.responses=inboundResponses;

      shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+coreReqLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
        var result = output.toString().replace(/\r?\n|\r/g, "");
        searchStrings.forEach(function(string) {
          result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
        });
        var coreRequests=result.replace(new RegExp("REQ", 'g'),"<br><span style='background:#5cb85c'>REQ</span>");
        response.core.requests=coreRequests;
    
        shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+coreResLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
          var result = output.toString().replace(/\r?\n|\r/g, "");
          searchStrings.forEach(function(string) {
            result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
          });
          result=result.replace(new RegExp("Success", 'g'),"<span style='color:green;font-weight: bold;'>Success</span>");              
          var coreResponses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
          response.core.responses=coreResponses;

          shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+billerReqLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
            var result = output.toString().replace(/\r?\n|\r/g, "");
            searchStrings.forEach(function(string) {
              result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
            });
            var billerRequests=result.replace(new RegExp("REQ", 'g'),"<br><span style='background:#5cb85c'>REQ</span>");
            response.biller.requests=billerRequests;
        
            shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+billerResLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
              var result = output.toString().replace(/\r?\n|\r/g, "");
              searchStrings.forEach(function(string) {
                result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
              });
              result=result.replace(new RegExp("Success", 'g'),"<span style='color:green;font-weight: bold;'>Success</span>");
              var billerResponses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
              response.biller.responses=billerResponses;

              shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+aggReqLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
                var result = output.toString().replace(/\r?\n|\r/g, "");
                searchStrings.forEach(function(string) {
                  result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
                });
                var aggregatorRequests=result.replace(new RegExp("REQ", 'g'),"<br><span style='background:#5cb85c'>REQ</span>");
                response.aggregator.requests=aggregatorRequests;
            
                shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+aggResLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
                  var result = output.toString().replace(/\r?\n|\r/g, "");
                  searchStrings.forEach(function(string) {
                    result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
                  });
                  result=result.replace(new RegExp("Success", 'g'),"<span style='color:green;font-weight: bold;'>Success</span>");
                  var aggregatorResponses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
                  response.aggregator.responses=aggregatorResponses;

                   shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+thirdPartyReqLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
                    var result = output.toString().replace(/\r?\n|\r/g, "");
                    result=result.replace(new RegExp("<", 'g'),"&lt;").replace(new RegExp(">", 'g'),"&gt;");
                    searchStrings.forEach(function(string) {
                      result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
                    });
                    var thirdPartyRequests=result.replace(new RegExp("REQ", 'g'),"<br><span style='background:#5cb85c'>REQ</span>");
                    response.thirdParty.requests=thirdPartyRequests;
                
                    shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+thirdPartyResLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
                      var result = output.toString().replace(/\r?\n|\r/g, "");
                      result=result.replace(new RegExp("<", 'g'),"&lt;").replace(new RegExp(">", 'g'),"&gt;");
                      searchStrings.forEach(function(string) {
                        result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
                      });
                      result=result.replace(new RegExp("Success", 'g'),"<span style='color:green;font-weight: bold;'>Success</span>");
                      var thirdPartyResponses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
                      response.thirdParty.responses=thirdPartyResponses;
                      
                      shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+callBackReqLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
                        var result = output.toString().replace(/\r?\n|\r/g, "");
                        searchStrings.forEach(function(string) {
                          result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
                        });
                        var callBackRequests=result.replace(new RegExp("REQ", 'g'),"<br><span style='background:#5cb85c'>CALLBACK_REQ</span>");
                        response.callBack.requests=callBackRequests;
                    
                        shell.exec("LC_ALL=C grep -E '"+searchStr+"' "+callBackResLogsPath+datePart+ "| sort -t '|' -k3,3", {silent:true}, function(status, output) {
                          var result = output.toString().replace(/\r?\n|\r/g, "");
                          searchStrings.forEach(function(string) {
                            result=result.replace(new RegExp(string, 'g'),"<span style='background:yellow'>"+string+"</span>");
                          });
                          result=result.replace(new RegExp("Success", 'g'),"<span style='color:green;font-weight: bold;'>Success</span>");
                          var callBackResponses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>CALLBACK_RES</span>");
                          response.callBack.responses=callBackResponses;
                          res.json(response);
                        });
                      });
                    });
                  });
                  
                });
              });

            });
          });

        });
      });

    });
  });

});

router.post(basePath+'/failed/logs', (req, res) => {
  var searchFor=req.body.for;
  var response={
     responses:[]
  };

var currentDate=new Date().toISOString().
  replace(/T/, ' ').      // replace T with a space
  replace(/\..+/, '');
var datePart=currentDate.split(" ")[0];

  var path=null;
  if(searchFor=='Core'){
    path=coreResLogsPath;
  }else if(searchFor=='Biller'){
    path=billerResLogsPath;
  }else if(searchFor=='Aggregator'){
    path=aggResLogsPath;
  }else if(searchFor=='ThirdParty'){
    path=thirdPartyResLogsPath;
  }else if(searchFor=='Inbound'){
    path=inboundResLogsPath;
  }else if(searchFor=='GatewayTimeout'){
    shell.exec("LANG=C grep -ir 'gateway time' --include '*re*.'"+datePart+"-* "+logsBasePath+"/", {silent:true}, function(status, output) {
      var result = output.toString().replace(/\r?\n|\r/g, "");
      result=result.replace(new RegExp("Gateway Time-out", 'g'),"<span style='color:red;font-weight: bold;'>Gateway Time-out</span>");
      var responses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
      response.responses=responses;
      res.json(response);
    });
  }else if(searchFor=='CurrentHour'){
    var nDate = new Date().toLocaleString('en-US', {
      timeZone: 'Asia/Jakarta'
    });
    var currentHour=parseInt(nDate.split(" ")[1].split(":")[0]);
    if(currentHour!=12 && nDate.split(" ")[2]=="PM"){
      currentHour+=12;
    }

    shell.exec("LANG=C grep -vi 'Success' "+coreResLogsPath+"."+datePart+"-"+currentHour, {silent:true}, function(status, output) {
      var result = output.toString().replace(/\r?\n|\r/g, "");
      result=result.replace(new RegExp("Failure", 'g'),"<span style='color:red;font-weight: bold;'>Failure</span>");
      result=result.replace(new RegExp("Failed", 'g'),"<span style='color:red;font-weight: bold;'>Failed</span>");
      var responses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
      response.responses=responses;
      res.json(response);
    });
  }else if(searchFor.indexOf("SaldoMobo") > -1){
    var errorCode=searchFor.split("-")[1];
    
    shell.exec("LANG=C grep '<res:ResultCode>"+errorCode+"</res:ResultCode>' "+saldomoboResLogsPath+"."+datePart+"-*", {silent:true}, function(status, output) {
      var result = output.toString().replace(/\r?\n|\r/g, "");
      result=result.replace(new RegExp('<res:ResultCode>'+errorCode+'</res:ResultCode>', 'g'),"<span style='color:red;font-weight: bold;'><res:ResultCode>"+errorCode+"</res:ResultCode></span>");
      result=result.replace(new RegExp("<", 'g'),"&lt;").replace(new RegExp(">", 'g'),"&gt;");
      var responses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
      response.responses=responses;
      res.json(response);
    });
  }else if(searchFor.indexOf("Bimasakti") > -1){
    var errorCode=searchFor.split("-")[1];
    shell.exec("LANG=C grep '<string>"+errorCode+"</string>' "+bimasaktiResLogsPath+"."+datePart+"-*", {silent:true}, function(status, output) {
      var result = output.toString().replace(/\r?\n|\r/g, "");
      result=result.replace(new RegExp('<string>'+errorCode+'</string>', 'g'),"<span style='color:red;font-weight: bold;'><string>"+errorCode+"</string></span>");
      result=result.replace(new RegExp("<", 'g'),"&lt;").replace(new RegExp(">", 'g'),"&gt;");
      var responses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
      response.responses=responses;
      res.json(response);
    });
  }

  if(path!=null){  
   shell.exec("LANG=C grep -vi 'Success' "+path+"."+datePart+"-*", {silent:true}, function(status, output) {
      var result = output.toString().replace(/\r?\n|\r/g, "");
      result=result.replace(new RegExp("Failure", 'g'),"<span style='color:red;font-weight: bold;'>Failure</span>");
      result=result.replace(new RegExp("Failed", 'g'),"<span style='color:red;font-weight: bold;'>Failed</span>");
      var responses=result.replace(new RegExp("RES", 'g'),"<br><span style='background:#5cb85c'>RES</span>");
      response.responses=responses;
      res.json(response);
    });
  }
  });
  

module.exports = router;
